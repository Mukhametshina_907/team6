from django.db import models
from django.contrib.auth.models import User


class Task(models.Model):
    # кто должен сделать задание
    name = models.ForeignKey(User, on_delete=models.CASCADE)
    # срок сдачи
    deadLine = models.DateField()
    # письменное описание задания
    task = models.TextField()
    # на сколько сделано задание
    progerssBar = models.PositiveSmallIntegerField(default=0)
    # комнда где находится задание
    team = models.ForeignKey('AllTeams', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.name) + ' ' + str(self.task) + ' ' + str(self.progerssBar)


class AllTeams(models.Model):
    # уникальный url для доски задний
    url = models.SlugField(unique=True)
    # название команды
    name = models.CharField("Назавание комнады: ", max_length=32)
    # все участники доски
    dashboards = models.ManyToManyField(User)

    def __str__(self):
        return str(self.url) + ' ' + str(self.name) + ' ' + str(self.dashboards.all())

    class Meta:
        verbose_name = 'Dashboard'
        verbose_name_plural = 'Dashboards'
