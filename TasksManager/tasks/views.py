import random

from django.http import HttpResponseRedirect
from django.contrib.auth.models import User, Group
from django.shortcuts import render, redirect, get_object_or_404
from .forms import TaskForm, NameForm, AllTeamsForm, Pseudo
from .models import Task, AllTeams
from django.contrib.auth.hashers import make_password
from django.utils import timezone
from django.core.exceptions import PermissionDenied
import datetime

def dashboards(request, team_url):
    if not request.user.is_authenticated:
        return redirect('/login')
    all_task_list = Task.objects.filter(team__url=team_url)
    name_list = AllTeams.objects.get(url=team_url).dashboards.all()
    # Добавление задачи
    form = TaskForm(request.POST or None)
    if request.method == "POST" and form.is_valid():

        q = Task(name=form.cleaned_data['name'],
                 deadLine=form.cleaned_data['deadLine'],
                 task=form.cleaned_data['task'],
                 progerssBar=form.cleaned_data['progerssBar'],
                 team=AllTeams.objects.get(url=team_url)
                 )
        q.save()
        return redirect('/dashboard/' + team_url + "/")
    else:
        form = TaskForm()

    # Все доски
    dashboards_list = AllTeams.objects.filter(dashboards__username=request.user.username)

    # Создание доски
    dashboard = AllTeamsForm(request.POST or None)
    if request.method == "POST" and dashboard.is_valid():
        d_board = AllTeams(url=dashboard.cleaned_data['url'], name=dashboard.cleaned_data['name'])
        q = request.user
        d_board.save()
        d_board.dashboards.add(q)
        return redirect('/dashboard/' + d_board.url + "/")

    # Присоединение к команде
    if request.method == "POST":
        r = request.user
        d_url = request.POST['join_url']
        try:
            d = AllTeams.objects.get(url=d_url)
            AllTeams.objects.get(url=d_url).dashboards.add(r.id)
            return redirect('/dashboard/' + d_url + "/")
        except AllTeams.DoesNotExist:
            return redirect('/dashboard/' + team_url + "/")
    return render(request, 'tasks/main.html',
                  {'all_task_list': all_task_list,
                   'form': form, 'name_list': name_list,
                   'dashboards_list': dashboards_list,
                   'user': request.user,
                   'dashform': dashboard,
                   'team': AllTeams.objects.get(url=team_url),
                   'now': timezone.now().date()
                   })


def edit(request, task_id):
    taskobj = get_object_or_404(Task, id=task_id)
    name_list = AllTeams.objects.get(url=taskobj.team.url).dashboards.all()
    form = TaskForm(request.POST or None, instance=taskobj)
    if request.method == "POST" and form.is_valid():
        form.save()
        return redirect('/dashboard/' + taskobj.team.url + "/")
    else:
        form = TaskForm(instance=taskobj)
    return render(request, 'tasks/edit.html', {'form': form, 'name_list': name_list, 'task': taskobj})


def profile(request):
    if not request.user.is_authenticated:
        return redirect("/login")
    dashboards_list = AllTeams.objects.filter(dashboards__username=request.user.username)
    try:
        if request.method == "POST":
            # Проверка на правильность ввода url
            check = AllTeams.objects.get(url=request.POST['url'])
            return redirect('/dashboard/' + request.POST['url'] + "/" + "join/")
    except AllTeams.DoesNotExist:
        return redirect("/me")
    return render(request, 'tasks/profile.html',
                  {'dashboards_list': dashboards_list, 'user': request.user})


def create(request):
    error = None
    if request.method == "POST":
        if not User.objects.filter(username=request.POST['username']):
            # Создание юзера по обработке пост запроса
            q = User(username=request.POST['username'],
                     first_name=request.POST['first_name'],
                     last_name=request.POST['last_name'], )
            q.password = make_password(password=request.POST['password'])
            q.id = random.randint(0, 1e10)
            q.save()
            # Юзер - обычный пользователь сайта
            q.groups.add(Group.objects.get(name="Member"))
            return redirect('/login')
        else:
            # если имя пользователя занято то высветим ошибку
            error = "Никнейм занят"
    return render(request, 'tasks/index.html', {'error_message': error})


def dashboard_add(request):
    if not request.user.is_authenticated:
        return redirect('/login')
    dashboard = AllTeamsForm(request.POST or None)
    if request.method == "POST" and dashboard.is_valid():
        d_board = AllTeams(url=dashboard.cleaned_data['url'], name=dashboard.cleaned_data['name'])
        q = request.user
        d_board.save()
        d_board.dashboards.add(q.id)
        return redirect('/dashboard/' + d_board.url + "/")
    return render(request, 'tasks/create_dash.html', locals())


def dashboard_join(request, d_url):
    if not request.user.is_authenticated:
        return redirect("/login")
    r = request.user  # пользователь
    d = AllTeams.objects.get(url=d_url)  # доска
    # проверка проходит на странице ввода url
    AllTeams.objects.get(url=d_url).dashboards.add(r)
    return redirect('/dashboard/' + d_url + "/")

def delete_task(request, task_id):
    if not request.user.is_authenticated:
        return redirect("/login")
    task = get_object_or_404(Task, id=task_id)
    team = task.team
    task.delete()
    return redirect('/dashboard/' + team.url + "/")


def redir(request):
    if request.user.is_authenticated:
        return redirect('/me/')
    else:
        return redirect('/login/')


# Выход из команды
def exit_team(request, exit_url):
    if not request.user.is_authenticated:
        return redirect("/login")
    r = request.user
    AllTeams.objects.get(url=exit_url).dashboards.remove(r.id)
    if AllTeams.objects.filter(dashboards__username=r.username):
        first_team = AllTeams.objects.filter(dashboards__username=r.username)[0].url
        return redirect("/dashboard/" + first_team)
    else:
        return redirect("/me/")


def done(request, task_id):
    if not request.user.is_authenticated:
        return redirect("/login")
    task = Task.objects.get(id=task_id)
    if not task.team.dashboards.filter(username=request.user.username):
        raise PermissionDenied
    task.progerssBar = 100
    task.save()
    return redirect('/dashboard/' + task.team.url)
