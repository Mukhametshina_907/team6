from django.urls import path, include
from . import views

urls_ = [
    path('', views.redir),
    path('signin/', views.create),
    path('dashboard/add/', views.dashboard_add),
    path('dashboard/<str:team_url>/', views.dashboards),
    path('tasks/<int:task_id>/edit/', views.edit),
    path('tasks/<int:task_id>/delete/', views.delete_task),
    path('<str:exit_url>/exit/', views.exit_team),
    path('tasks/<int:task_id>/done/', views.done),
    path('me/', views.profile),
    path('', include('django.contrib.auth.urls')),
    path('dashboard/<str:d_url>/join/', views.dashboard_join),
]
urlpatterns = urls_
