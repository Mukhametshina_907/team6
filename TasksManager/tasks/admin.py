from django.contrib import admin
from tasks.models import Task, AllTeams


class TaskAdmin (admin.ModelAdmin):
    list_display = ('id', 'name', 'team', 'deadLine')


class PersonAdmin (admin.ModelAdmin):
    list_display = ('id', 'username')


admin.site.register(Task, TaskAdmin)
admin.site.register(AllTeams)


