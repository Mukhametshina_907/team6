from django import forms
from .models import Task, AllTeams


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        exclude = ["team"]


class AllTeamsForm(forms.ModelForm):
    class Meta:
        model = AllTeams
        exclude = ["dashboards"]


class NameForm(forms.Form):
    name = forms.CharField()


class Pseudo(forms.Form):
    name = forms.CharField(label="name")
    task = forms.CharField(label="task")
    deadLine = forms.CharField(label="deadLine")
    progressBar = forms.IntegerField(label="progressBar")

