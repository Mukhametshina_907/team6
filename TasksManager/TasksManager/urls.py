from django.contrib import admin
from django.urls import path, include

urls_ = [
    path('admin/', admin.site.urls),
    path('', include('tasks.urls')),
    path('dashboard/', include('tasks.urls')),
    path('.*/', include('tasks.urls'))
]
urlpatterns = urls_

